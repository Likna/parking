public class Parking {

    public Parking(int carCapacity, int lorryCapacity) {
        this.maxNumberOfCars = carCapacity;
        this.maxNumberOfLorries = lorryCapacity;
        this.numberOfFreePlacesForCars = this.maxNumberOfCars;
        this.numberOfFreePlacesForLorries = this.maxNumberOfLorries;
        this.placeForC = new ParkingPlace[carCapacity];
        this.placeForL = new ParkingPlace[lorryCapacity];

        do {
            --carCapacity;
            --lorryCapacity;
            if (carCapacity >= 0) {
                this.placeForC[carCapacity] = new ParkingPlace();
            }

            if (lorryCapacity >= 0) {
                this.placeForL[lorryCapacity] = new ParkingPlace();
            }
        } while(carCapacity >= 0 || lorryCapacity >= 0);

    }

    private final int maxNumberOfCars;
    private final int maxNumberOfLorries;
    ParkingPlace[] placeForC;
    ParkingPlace[] placeForL;

    public int capacity(){return maxNumberOfCars + maxNumberOfLorries;}

    private int numberOfFreePlacesForCars;
    private int numberOfFreePlacesForLorries;

    public int getNumberOfFreePlacesForCars() {
        return this.numberOfFreePlacesForCars;
    }

    public int getNumberOfFreePlacesForLorries() {
        return numberOfFreePlacesForLorries + findPlaceForLorry();
    }

    public int getNumberOfFreePlacesOnlyForLorries(){return numberOfFreePlacesForLorries;}

    public int getMaxNumberOfCars() {
        return this.maxNumberOfCars;
    }

    public int getMaxNumberOfLorries() {
        return this.maxNumberOfLorries;
    }

    public void findFreePlace(Car car, boolean isCar){
        if(isCar) {
            findFreePlace(car, this.placeForC);
            this.numberOfFreePlacesForCars-=1;
        }
        else {
            if(hasFreePlaces(this.numberOfFreePlacesForLorries, this.maxNumberOfLorries, this.placeForL)) {
                findFreePlace(car, this.placeForL);
                this.numberOfFreePlacesForLorries -= 1;
            }
            else findPlaceForLorry(car);
        }
    }

    private void findFreePlace(Car car, ParkingPlace[] place) {
        int i = 0;
        while (place[i].hasCar())
            i++;
        place[i].setCar(car);
    }

    private void findPlaceForLorry(Car car){
        for (int i = 0; i < this.maxNumberOfCars-1; i++) {
            if(!(placeForC[i].hasCar()||placeForC[i+1].hasCar())){
                placeForC[i].setCar(car);
                placeForC[i + 1].setCar(car, placeForC[i].getNumberOfTimes());
                this.numberOfFreePlacesForCars -= 2;
                break;
            }
        }
    }

    private int findPlaceForLorry(){
        int tmp = 0;
        for (int i = 0; i < this.maxNumberOfCars-1; i++) {
            if(!(placeForC[i].hasCar()||placeForC[i+1].hasCar())) {
                tmp++;
                i++;
            }
        }
        return tmp;
    }


    public void increaseTimes(){
        increaseTimesForDiffKind(this.placeForC, this.maxNumberOfCars, true);
        increaseTimesForDiffKind(this.placeForL, this.maxNumberOfLorries, false);
    }

    private void increaseTimesForDiffKind(ParkingPlace[] place, int max, boolean isCar) {
        for(int i = 0; i < max; ++i) {
            if (place[i].hasCar()) {
                place[i].increaseTime();
                if(!place[i].hasCar()) {
                    if(isCar)
                        this.numberOfFreePlacesForCars++;
                    else
                        this.numberOfFreePlacesForLorries++;
                }
            }
        }
    }

    private boolean flag = false;

    public boolean hasFreePlaces(boolean isForCar){
        if(isForCar)
            return hasFreePlaces(this.numberOfFreePlacesForCars, this.maxNumberOfCars, this.placeForC);
        else {
            this.flag = hasFreePlaces(this.numberOfFreePlacesForLorries, this.maxNumberOfLorries, this.placeForL);
            if(!flag) {
                return hasFreePlacesInCForL(this.maxNumberOfCars);
            }
            return true;
        }
    }

    private boolean hasFreePlaces(int freePlaces, int max, ParkingPlace[] place) {
        if (freePlaces == max) {
            return true;
        } else {
            for(int i = 0; i < max; ++i) {
                if (!place[i].hasCar()) {
                    return true;
                }
            }
            return false;
        }
    }

    private boolean hasFreePlacesInCForL(int max){
        for(int i = 0; i < max-1; i++) {
            if (!(placeForC[i].hasCar()||placeForC[i+1].hasCar())) {
                return true;
            }
        }
        return false;
    }

    public int nearestPlace(boolean isCar){
        int tmp;
        if (isCar)
            tmp = nearestPlace(placeForC, maxNumberOfCars);
        else
            tmp = nearestPlace(placeForL, maxNumberOfLorries);
        return tmp;
    }

    private int nearestPlace(ParkingPlace[] place, int max) {
        int tmp = place[0].getNumberOfTimes();

        for(int i = 1; i < max; ++i) {
            if (place[i].getNumberOfTimes() != 0 && tmp > place[i].getNumberOfTimes()) {
                tmp = place[i].getNumberOfTimes();
            }
        }
        return tmp;
    }

    public void writeAllInformation(){
        writeInformation(placeForC, maxNumberOfCars, "car", "C");
        writeInformation(placeForL, maxNumberOfLorries, "lorry", "L");
    }

    private void writeInformation(ParkingPlace[] place, int max, String type, String letter){
        for (int i = 0; i < max; i++) {
            if(place[i].hasCar() && place[i].getCar().isLorry)
                type = "lorry";
            System.out.print(" Place " + letter + i + ": ");
            if(place[i].hasCar()){
                System.out.print(type + " ");
                place[i].writeCarNumber();
                System.out.println(" " + place[i].getNumberOfTimes());
            }
            else
                System.out.println("empty");
        }
    }
}
