public class ParkingPlace {
    private Car car;
    private int numberOfTimes;
    private int add;

    public int getNumberOfTimes() {
        return this.numberOfTimes;
    }

    public void setCar(Car car) {
        this.numberOfTimes = (int)(1 + Math.random() * 10);
        this.car = car;
    }

    public void setCar(Car car, int num) {
        this.numberOfTimes = num;
        this.car = car;
    }

    public Car getCar() {
        return this.car;
    }

    public void writeCarNumber(){
        if(hasCar())
            car.writeNumber();
    }

    public void increaseTime() {
        --this.numberOfTimes;
    }

    public boolean hasCar() {
        return this.numberOfTimes != 0;
    }
}
