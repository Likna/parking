import java.util.Scanner;
//@TODO: add working with lorry
public class Manager {
    Scanner scn;
    int capacity;
    int p = 0;
    Parking parking;

    public Manager(int numbC, int numbL, Scanner scanner) {
        this.capacity = numbC + numbL;
        this.scn = scanner;
        this.parking = new Parking(numbC, numbL);
    }

    public String work(int freePlace, boolean isCar, String type){
        if (parking.hasFreePlaces(isCar)){
            int n = (int)(1 + Math.random() * (double)(this.parking.capacity() / 3));

            if(freePlace>=n) {
                Car[] car = new Car[n];
                for (int i = 0; i < n; i++) {
                    if(isCar)
                        car[i] = new Car();
                    else
                        car[i] = new Car(true);
                    this.parking.findFreePlace(car[i], isCar);
                }
                return "Added " + n + " " + type;
            }
            else {
                Car[] car = new Car[freePlace];
                for (int j = 0; j < freePlace; ++j) {
                    if(isCar)
                        car[j] = new Car();
                    else
                        car[j] = new Car(true);
                    this.parking.findFreePlace(car[j], isCar);
                }
                return "Added " + freePlace + type + "\nSorry, it's not enough spaces for " + (n - freePlace) + " "+ type +
                        "\nNearest space will be released in " + this.parking.nearestPlace(isCar) + " times";
            }

        }
        else
            return "Sorry, there is no parking spaces \n Nearest space will be released in " + this.parking.nearestPlace(true) + " times";
    }

    void trial() {
        do {
            this.parking.increaseTimes();
            System.out.println(this.work(this.parking.getNumberOfFreePlacesForCars(),true,"cars"));
            System.out.println(this.work(this.parking.getNumberOfFreePlacesForLorries(),false,"lorries"));
            System.out.println();
            this.inscription();
        } while(this.p != 5);

    }

    void inscription() {
        System.out.println("Next time - 1"+
                "\nHow many spaces are free, taken, when is the nearest free space - 2"+
                "\nGet full information about parking places - 3"+
                "\nClean the parking and start next time - 4"+
                "\nCreate new parking - 5"+
                "\nStop - 6");
        this.p = this.scn.nextInt();
        switch(this.p) {
            case 1:
                this.p = 1;
                break;
            case 2:
                System.out.println("Free car spaces: " + this.parking.getNumberOfFreePlacesForCars()+
                        "\nTaken car spaces: " + (this.parking.getMaxNumberOfCars() - this.parking.getNumberOfFreePlacesForCars()));
                System.out.println("Free lorry spaces: " + this.parking.getNumberOfFreePlacesOnlyForLorries()+
                        "\nTaken lorry spaces: " + (this.parking.getMaxNumberOfLorries() - this.parking.getNumberOfFreePlacesForLorries()));
                System.out.println("Nearest place will be released in " + this.parking.nearestPlace(true) + " times\n");
                this.inscription();
                break;
            case 3:
                parking.writeAllInformation();
                this.inscription();
                break;
            case 4:
                int c = this.parking.getMaxNumberOfCars();
                int l = this.parking.getMaxNumberOfLorries();
                this.parking = new Parking(c, l);
                break;
            case 5:
                System.out.println("Please, enter the capacity of car parking");
                System.out.println("Car capacity: ");
                int car = scn.nextInt();
                System.out.println("Lorry capacity: ");
                int lorry = scn.nextInt();
                this.parking = new Parking(car, lorry);
                break;
            case 6:
                this.p = 5;
                break;
            default:
                System.out.println("Please, enter number from 1 to 5");
                this.inscription();
        }

    }

    public static void main(String[] args) {
        System.out.println("Please, enter the capacity of car parking");
        Scanner scn = new Scanner(System.in);
        System.out.println("Car capacity: ");
        int numbC = scn.nextInt();
        System.out.println("Lorry capacity: ");
        int numbL = scn.nextInt();
        Manager man = new Manager(numbC, numbL, scn);
        man.trial();
    }
}
