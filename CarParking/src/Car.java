public class Car {
    public boolean isLorry=false;
    private final char[] number;

    public Car() {
        char[] num = new char[6];
        int firstL = 65;
        int lastL = 90;
        int firstN = 48;
        int lastN = 57;

        for(int i = 0; i < 6; ++i) {
            int rand;
            if (i != 1 && i != 2 && i != 3) {
                rand = firstL + (int)(Math.random() * (double)(lastL - firstL));
            } else {
                rand = firstN + (int)(Math.random() * (double)(lastN - firstN));
            }

            num[i] = (char)rand;
        }

        this.number = num;
    }

    public Car(boolean Lorry) {
        this();
        this.isLorry = Lorry;
    }

    public void writeNumber() {
        System.out.print(this.number);
    }
}